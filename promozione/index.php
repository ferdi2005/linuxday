<?php
/*
Copyright (C) 2019  Italian Linux Society - http://www.linux.it

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

require_once ('../funzioni.php');
lugheader ('Linux Day ' . conf('current_year') . ': Promozione');

?>

<h2>Promozione</h2>

<p>
	Aiutaci anche tu a diffondere la notizia del Linux Day!
</p>
<p>
	Se hai un sito o un blog online e vuoi informare i tuoi visitatori sull'evento del 28 ottobre, usa il codice HTML sotto pubblicato per includere uno dei banner a disposizione o <a href="<?php echo makeurl('/promo/webkit.zip') ?>">scarica qui l'archivio completo</a>, e qua <a href="<?php echo makeurl('/immagini/linuxday_fullcolor.svg') ?>">il logo in formato SVG</a>.
</p>

<table class="table">
	<tr>
		<td>
			<p>180 x 150</p>

			<p>
				<img src="<?php echo makeurl('/promo/banner_180x150.png') ?>" class="banner img-fluid" alt="Linux Day 2019">
			</p>

			<pre>
&lt;a href="https://www.linuxday.it"&gt;
  &lt;img src="<?php echo makeurl('/promo/banner_180x150.png') ?>"
  border="0" alt="Linux Day <?php echo conf('current_year') ?>" /&gt;
&lt;/a&gt;
</pre>
		</td>
	</tr>
	<tr>
		<td>
			<p>300 x 250</p>

			<p>
				<img src="<?php echo makeurl('/promo/banner_300x250.png') ?>" class="banner img-fluid" alt="Linux Day 2019">
			</p>

			<pre>
&lt;a href="https://www.linuxday.it"&gt;
  &lt;img src="<?php echo makeurl('/promo/banner_300x250.png') ?>"
  border="0" alt="Linux Day <?php echo conf('current_year') ?>" /&gt;
&lt;/a&gt;
</pre>
		</td>
	</tr>
	<tr>
		<td>
			<p>468 x 60</p>

			<p>
				<img src="<?php echo makeurl('/promo/banner_468x60.png') ?>" class="banner img-fluid" alt="Linux Day 2019">
			</p>

			<pre>
&lt;a href="https://www.linuxday.it"&gt;
  &lt;img src="<?php echo makeurl('/promo/banner_468x60.png') ?>"
  border="0" alt="Linux Day <?php echo conf('current_year') ?>" /&gt;
&lt;/a&gt;
</pre>
		</td>
	</tr>
	<tr>
		<td>
			<p>120 x 600</p>

			<p>
				<img src="<?php echo makeurl('/promo/banner_120x600.png') ?>" class="banner img-fluid" alt="Linux Day 2019">
			</p>

			<pre>
&lt;a href="https://www.linuxday.it"&gt;
  &lt;img src="<?php echo makeurl('/promo/banner_120x600.png') ?>"
  border="0" alt="Linux Day <?php echo conf('current_year') ?>" /&gt;
&lt;/a&gt;
</pre>
		</td>
	</tr>
	<tr>
		<td>
			<p>Immagine Facebook</p>

			<p>
				<img src="<?php echo makeurl('/promo/banner_facebook.png') ?>" class="banner img-fluid" alt="Linux Day 2019">
			</p>

			<pre>
&lt;a href="https://www.linuxday.it"&gt;
  &lt;img src="<?php echo makeurl('/promo/banner_facebook.png') ?>"
  border="0" alt="Linux Day <?php echo conf('current_year') ?>" /&gt;
&lt;/a&gt;
</pre>
		</td>
	</tr>
</table>

<?php
lugfooter ();
