<?php
require_once ('../funzioni.php');
lugheader ('Linux Day ' . conf('current_year') . ': Modifica evento admin',
	['https://unpkg.com/leaflet@1.5.1/dist/leaflet.css', makeurl('/registra/registra.css')],
	['https://unpkg.com/leaflet@1.5.1/dist/leaflet.js', makeurl('/registra/registra.js')]
);

	if (isset($_SESSION['admin']) && $_SESSION['admin'] == 'S') {
		if(!isset($_POST['action']) && isset($_GET['email'])) {
		$events_file = '../data/events' . conf('current_year') . '.json';
		$event = findEvent($events_file, $_GET['email']);
?>
	<form method="POST" action="<?php echo makeurl('/admin/modifica.php') ?>">
		<input type="hidden" name="action" value="save">
		<?php if(!empty($message)): ?>
			<div class="alert alert-info">
				<?php echo $message ?>
			</div>
		<?php endif ?>

		<div class="form-group">
			<label for="group">Organizzatore</label>
			<input type="text" class="form-control" id="group" name="group" value="<?php echo $event->group ?>" required>
			<small class="form-text text-muted">Nome del gruppo organizzatore (LUG, associazione, scuola, ente...). Non il tuo nome e cognome!</small>
		</div>
		<div class="form-group">
			<label for="city">Città</label>
			<input type="text" class="form-control" id="city" name="city" value="<?php echo $event->city ?>" required>
		</div>
		<div class="form-group">
			<label for="city">Provincia</label>
			<?php prov_select('form-control', $event->prov) ?>
		</div>
		<div class="form-group">
			<label for="web">Sito Web</label>
			<input type="text" class="form-control" id="web" name="web" value="<?php echo $event->web ?>" required>
			<small class="form-text text-muted">URL della pagina su cui reperire informazioni sull'evento (e.g. http://miolug.it/linuxday). Non usate la homepage del vostro sito, piuttosto create una pagina temporanea da arricchire in seguito!</small>
		</div>
		<div class="form-group">
			<input type="hidden" name="coordinates" value="<?php echo $event->coords ?>">
			<label for="mapid">Mappa</label>
			<div id="mapid"></div>
			<small class="form-text text-muted">Posiziona il marker cliccando sulla mappa, per indicare il luogo dove si svolgerà il tuo evento. Sii il più preciso possibile!</small>
		</div>
		<div class="form-group form-check">
			<input type="checkbox" class="form-check-input" id="gadgets" name="gadgets" <?php echo ($event->gadgets ? 'checked' : '') ?>>
			<label class="form-check-label" for="gadgets">Richiesta Materiali</label>
			<small class="form-text text-muted">Ogni anno Italian Linux Society invia agli organizzatori del Linux Day un pacco di spille, adesivi e materialie informativo. Spunta questa casella e compila il campo sotto per riceverlo anche tu!</small>
		</div>
		<div class="form-group">
			<label for="gadgets_address">Indirizzo Recapito</label>
			<textarea class="form-control" id="gadgets_address" name="gadgets_address" rows="7"><?php echo $event->gadgets_address ?></textarea>
		</div>
		
		<div class="form-check">
			<input type="checkbox" id="checkbox_approvato" name="checkbox_approvato" <?php echo ($event->approvato ? 'checked' : '') ?>>
			<label class="form-check-label" for="exampleCheck1">Evento approvato e da pubblicare?</label>
		</div>
		
		<input type="hidden" id="owner" name="owner" value="<?php echo($event->owner) ?>" >
		<button type="submit" class="btn btn-primary">Salva</button>
	</form>

<?php
	}
	if ($_SESSION['admin'] == 'S' && isset($_POST['action']) && $_POST['action'] == 'save') {
			$events_file = '../data/events' . conf('current_year') . '.json';
			$event = findEvent($events_file, $_POST['owner']);
			$event->group = $_POST['group'];
			$event->city = $_POST['city'];
			$event->prov = $_POST['prov'];
			$event->web = $_POST['web'];
			$event->coords = $_POST['coordinates'];
			$event->gadgets = isset($_POST['gadgets']);
			$event->gadgets_address = $_POST['gadgets_address'];
			if (isset($_POST['checkbox_approvato']) && $_POST['checkbox_approvato'] == 'on') {
				$event->approvato = true;
				$text = "Grazie! L'evento che hai organizzato per il LinuxDay è stato approvato ed ora è visibile sul sito! Ci vediamo il" . $human_date;

				$headers = 'From: webmaster@linux.it' . "\r\n";
				mail($event->owner, 'Approvazione evento Linux ay', $text, $headers);
			}
			$result = saveEvent($events_file, $event);
			$message = 'Evento salvato correttamente! Stai per essere reindirizzato alla dashboard. <meta http-equiv="refresh" content="5;URL="'. makeurl('admin/index.php') . '">';

	}
		
	} else {
?>
		<div class="alert alert-danger">
			Pagina riservata agli admin
		</div>
<?php
	}
?>
	<?php if(isset($message)): ?>
		<div class="alert alert-success">
			<?php echo($message); ?>
		</div>
	<?php endif ?>