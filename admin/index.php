<?php
/*
Copyright (C) 2019  Italian Linux Society - http://www.linux.it

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
	require_once ('../funzioni.php');
	lugheader ('Linux Day ' . conf('current_year') . ': Admin');
	
?>

<h1 class="h1 title">Eventi da approvare</h1>
<table class="table">
	<thead>
		<tr>
			<th scope="col">Gruppo</th>
			<th scope="col">Città</th>
			<th scope="col">Provincia</th>
			<th scope="col">Sito web</th>
			<th scope="col">Coordinate</th>
			<th scope="col">Owner</th>
			<th scope="col">Comandi</th>
		</tr>
	</thead>
	<tbody>
<?php
	if (isset($_SESSION['admin']) && $_SESSION['admin'] == 'S') {
		# Inizio elenco eventi
		$events_file = '../data/events' . conf('current_year') . '.json';
		$events = json_decode(file_get_contents($events_file));
		foreach($events as $e) {
			if ($e->approvato == false) {
?>
			<tr>
				<td style="width:100px;word-break: break-word;"><?php echo($e->group) ?></td>
				<td style="width:100px;word-break: break-word;"><?php echo($e->city) ?></td>
				<td style="width:100px;word-break: break-word;"><?php echo($e->prov) ?></td>
				<td style="width:100px;word-break: break-word;"><?php echo($e->web) ?></td>
				<td style="width:100px;word-break: break-word;"><?php echo($e->coords) ?></td>
				<td style="width:100px;word-break: break-word;"><?php echo($e->owner) ?></td>
				<td><a href='/admin/modifica.php?email=<?php echo($e->owner) ?>'> Modifica evento</a> </td> 
			</tr>
<?php
			}
		}
?>
	
	</tbody>
	</table>
	
	<h1 class="h1 title">Eventi approvati</h1>
	<table class="table">
	<thead>
		<tr>
			<th scope="col">Gruppo</th>
			<th scope="col">Città</th>
			<th scope="col">Provincia</th>
			<th scope="col">Sito web</th>
			<th scope="col">Coordinate</th>
			<th scope="col">Owner</th>
			<th scope="col">Comandi</th>
		</tr>
	</thead>
	<tbody>

<?php
		$events = json_decode(file_get_contents($events_file));
		foreach($events as $e) {
			if ($e->approvato == true) {
?>
			<tr>
				<td style="width:100px;word-break: break-word;"><?php echo($e->group) ?></td>
				<td style="width:100px;word-break: break-word;"><?php echo($e->city) ?></td>
				<td style="width:100px;word-break: break-word;"><?php echo($e->prov) ?></td>
				<td style="width:100px;word-break: break-word;"><?php echo($e->web) ?></td>
				<td style="width:100px;word-break: break-word;"><?php echo($e->city) ?></td>
				<td style="width:100px;word-break: break-word;"><?php echo($e->coords) ?></td>
				<td style="width:100px;word-break: break-word;"><?php echo($e->owner) ?></td>
				<td><a href='/admin/modifica.php?email=<?php echo($e->owner) ?>'> Modifica evento</a> </td> 
			</tr>
<?php
			}
		}
echo('</tbody>');
echo('</table>');
	} else {
?>
		<div class="alert alert-danger">
			Pagina riservata agli admin
		</div>
<?php
	}
?>
