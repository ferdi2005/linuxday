<?php
/*
Copyright (C) 2019  Italian Linux Society - http://www.linux.it

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

require_once ('../funzioni.php');
lugheader ('Linux Day ' . conf('current_year') . ': Registra Evento',
	['https://unpkg.com/leaflet@1.5.1/dist/leaflet.css', makeurl('/registra/registra.css')],
	['https://unpkg.com/leaflet@1.5.1/dist/leaflet.js', makeurl('/registra/registra.js')]
);

if (empty($_SESSION['user_email'])) {
	?>

	<div class="alert alert-danger">
		Questa pagina è riservata a chi ha un account registrato. <a href="<?php echo makeurl('/user') ?>">Vai su questa pagina</a> per autenticarti o registrarti.
	</div>

	<?php
}
else {
	$events_file = '../data/events' . conf('current_year') . '.json';
	$event = findEvent($events_file, $_SESSION['user_email']);
	$message = '';

	if (isset($_POST['action'])) {
		if ($_POST['action'] == 'save') {
			$event->group = $_POST['group'];
			$event->city = $_POST['city'];
			$event->prov = $_POST['prov'];
			$event->web = $_POST['web'];
			$event->coords = $_POST['coordinates'];
			$event->gadgets = isset($_POST['gadgets']);
			$event->gadgets_address = $_POST['gadgets_address'];
			$event->approvato = false;
			$result = saveEvent($events_file, $event);
			$message = 'Evento salvato correttamente!';
		}
	}

	?>

	<h2>Crea / Modifica Evento</h2>

	<?php if(!empty($message)): ?>
		<div class="alert alert-info">
			<?php echo $message ?>
		</div>
	<?php endif ?>

	<form method="POST" action="<?php echo makeurl('/registra/index.php') ?>">
		<input type="hidden" name="action" value="save">

		<div class="form-group">
			<label for="group">Organizzatore</label>
			<input type="text" class="form-control" id="group" name="group" value="<?php echo $event->group ?>" required>
			<small class="form-text text-muted">Nome del gruppo organizzatore (LUG, associazione, scuola, ente...). Non il tuo nome e cognome!</small>
		</div>
		<div class="form-group">
			<label for="city">Città</label>
			<input type="text" class="form-control" id="city" name="city" value="<?php echo $event->city ?>" required>
		</div>
		<div class="form-group">
			<label for="city">Provincia</label>
			<?php prov_select('form-control', $event->prov) ?>
		</div>
		<div class="form-group">
			<label for="web">Sito Web</label>
			<input type="text" class="form-control" id="web" name="web" value="<?php echo $event->web ?>" required>
			<small class="form-text text-muted">URL della pagina su cui reperire informazioni sull'evento (e.g. http://miolug.it/linuxday). Non usate la homepage del vostro sito, piuttosto create una pagina temporanea da arricchire in seguito!</small>
		</div>
		<div class="form-group">
			<input type="hidden" name="coordinates" value="<?php echo $event->coords ?>">
			<label for="mapid">Mappa</label>
			<div id="mapid"></div>
			<small class="form-text text-muted">Posiziona il marker cliccando sulla mappa, per indicare il luogo dove si svolgerà il tuo evento. Sii il più preciso possibile!</small>
		</div>
		<div class="form-group form-check">
			<input type="checkbox" class="form-check-input" id="gadgets" name="gadgets" <?php echo ($event->gadgets ? 'checked' : '') ?>>
			<label class="form-check-label" for="gadgets">Richiesta Materiali</label>
			<small class="form-text text-muted">Ogni anno Italian Linux Society invia agli organizzatori del Linux Day un pacco di spille, adesivi e materialie informativo. Spunta questa casella e compila il campo sotto per riceverlo anche tu!</small>
		</div>
		<div class="form-group">
			<label for="gadgets_address">Indirizzo Recapito</label>
			<textarea class="form-control" id="gadgets_address" name="gadgets_address" rows="7"><?php echo $event->gadgets_address ?></textarea>
		</div>
		<button type="submit" class="btn btn-primary">Salva</button>
	</form>

	<?php
}

lugfooter ();
