<?php
/*
Copyright (C) 2019  Italian Linux Society - http://www.linux.it

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

require_once ('funzioni.php');
lugheader ('Linux Day ' . conf('current_year'));

?>

<div class="highlight-box mb-5">
	<strong><?php echo conf('human_date') ?></strong> torna la principale manifestazione italiana dedicata a Linux, al software libero, alla cultura aperta ed alla condivisione: decine di eventi in tutta Italia, centinaia di volontari coinvolti e migliaia di visitatori per celebrare insieme la libertà digitale!
</div>
<p>
	Dal 2001 il Linux Day è una iniziativa distribuita per conoscere ed approfondire Linux ed il software libero. Si compone di numerosi eventi locali, organizzati autonomamente da gruppi di appassionati nelle rispettive città, tutti nello stesso giorno. In tale contesto puoi trovare talks, workshops, spazi per l'assistenza tecnica, gadgets, dibattiti e dimostrazioni pratiche.
</p>
<p>
	Tra settembre e ottobre in questa pagina saranno elencati tutte le manifestazioni cittadine aderenti: torna a visitare questa pagina nelle prossime settimane. Seguici su <a href="https://twitter.com/LinuxDayItalia">Twitter</a> o <a href="https://www.facebook.com/LinuxDayItalia">Facebook</a>, o <a href="http://www.ils.org/newsletter">iscriviti alla newsletter</a> per aggiornamenti.
</p>
<p>
	L'accesso al Linux Day è libero e gratuito!
</p>

<hr>

<p class="text-center">
	<img class="banner" src="<?php echo makeurl('/promo/banner_300x250.png') ?>" alt="Linux Day 2019">
</p>

<p>
	Artificial Intelligence, Machine Learning, Big Data... Utili strumenti che ci semplificano la vita, ma anche minacce per la privacy e l'individualità.
</p>
<p>
	Parliamone insieme al Linux Day! Perché nel mondo reale, contrariamente al mondo digitale, non è tutto 0 o 1.....
</p>

<?php
lugfooter ();
?>
