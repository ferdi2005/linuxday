<?php

/*
LinuxDay
Copyright (C) 2019  Italian Linux Society - http://www.linux.it

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

function conf($name) {
	require('config.php');
	if (!isset($$name)) {
		echo 'parametro ' . $name . ' non esistente';
	}
	return $$name;
}

function makeurl($url) {
	if (substr($url, 0, 1) != '/') {
		$url = '/' . $url;
	}
	return 'https://www.linuxday.it/' . conf('current_year') . $url;
	// return 'http://127.0.0.1:9000/' . $url;
	// return 'http://linuxday.local.it' . $url;
}

function random_string($length = 10) {
	$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$len = strlen($characters);
	$ret = '';

	for ($i = 0; $i < $length; $i++) {
		$ret .= $characters[rand(0, $len - 1)];
	}

	return $ret;
}

function findEvent($events_file, $current_user) {

	if (!empty($current_user) && file_exists($events_file)) {
		$events = json_decode(file_get_contents($events_file));
		foreach($events as $e) {
			if ($e->owner == $current_user) {
				return $e;
			}
		}
	}

	return (object) [
		'existing' => false,
		'owner' => $current_user,
		'group' => '',
		'city' => '',
		'prov' => null,
		'web' => '',
		'coords' => '42.204,11.711',
		'gadgets' => false,
		'gadgets_address' => '',
		'approvato' => false,
	];
}

function saveEvent($events_file, $event) {
	if (file_exists($events_file)) {
		$events = json_decode(file_get_contents($events_file));
	}
	else {
		$events = [];
	}

	if (isset($event->existing) && $event->existing == false) {
		unset($event->existing);
		$events[] = $event;
	}
	else {
		foreach($events as $index => $e) {
			if ($event->owner == $e->owner) {
				$events[$index] = $e;
				break;
			}
		}
	}

	file_put_contents($events_file, json_encode($events));
}

function lugheader ($title, $extracss = null, $extrajs = null) {
	session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="italian" />
	<meta name="robots" content="noarchive" />
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans|Nobile|Nobile:b" />
	<link href="<?php echo makeurl('/css/bootstrap.min.css') ?>" rel="stylesheet" type="text/css" />
	<link href="<?php echo makeurl('/css/main.css') ?>" rel="stylesheet" type="text/css" />

	<meta name="dcterms.creator" content="Italian Linux Society" />
	<meta name="dcterms.type" content="Text" />
	<link rel="publisher" href="http://www.ils.org/" />

	<meta name="twitter:title" content="Linux Day, <?php echo conf('human_date') ?>" />
	<meta name="twitter:creator" content="@LinuxDayItalia" />
	<meta name="twitter:card" content="summary" />
	<meta name="twitter:url" content="<?php echo makeurl('/') ?>" />
	<meta name="twitter:image" content="<?php echo makeurl('/immagini/tw.png') ?>" />

	<meta property="og:site_name" content="Linux Day" />
	<meta property="og:title" content="Linux Day, <?php echo conf('human_date') ?>" />
	<meta property="og:url" content="<?php echo makeurl('/') ?>" />
	<meta property="og:image" content="<?php echo makeurl('/immagini/fb.png') ?>" />
	<meta property="og:type" content="website" />
	<meta property="og:country-name" content="Italy" />
	<meta property="og:email" content="webmaster@linux.it" />
	<meta property="og:locale" content="it_IT" />
	<meta property="og:description" content="Giornata Nazionale per il Software Libero" />

	<script type="text/javascript" src="<?php echo makeurl('/js/jquery.slim.min.js') ?>"></script>

	<?php

	if ($extracss != null) {
		foreach ($extracss as $e) {
			?>
			<link href="<?php echo $e; ?>" rel="stylesheet" type="text/css" />
			<?php
		}
	}

	if ($extrajs != null) {
		foreach ($extrajs as $e) {
			?>
			<script type="text/javascript" src="<?php echo $e; ?>"></script>
			<?php
		}
	}

	?>

	<title><?php echo $title; ?></title>
</head>
<body>

<div id="header">
	<img src="<?php echo makeurl('/immagini/logo.png') ?>" width="79" height="79" alt="Linux Day" />
	<div id="maintitle">Linux Day <span class="hidden-sm"><?php echo conf('current_year') ?></span></div>
	<div id="payoff">Giornata Nazionale per il Software Libero</div>

	<div class="menu">
		<a class="generalink" href="<?php echo makeurl('/') ?>">Home</a>
		<a class="generalink" href="<?php echo makeurl('/howto/') ?>">Organizzati</a>
		<a class="generalink" href="<?php echo makeurl('/promozione/') ?>">Promozione</a>

		<?php if(!empty($_SESSION['user_email'])): ?>
			<a class="generalink" href="<?php echo makeurl('/registra/index.php') ?>">Il mio LinuxDay</a>
			<a class="generalink" href="<?php echo makeurl('/user/?action=logout') ?>">Logout</a>
		<?php else: ?>
			<a class="generalink" href="<?php echo makeurl('/user') ?>">Login</a>
		<?php endif ?>
		
		<?php if(isset($_SESSION['admin'])): ?>
			<a class="generalink" href="<?php echo makeurl('/admin/index.php') ?>">Admin</a>
		<?php endif ?>

		<p class="social mt-2">
			<a href="https://twitter.com/LinuxDayItalia"><img src="<?php echo makeurl('/immagini/twitter.png') ?>"></a>
			<a href="https://www.facebook.com/LinuxDayItalia"><img src="<?php echo makeurl('/immagini/facebook.png') ?>"></a>
		</p>
	</div>
</div>

<div class="container mt-5">
	<div class="row">
		<div class="col-md-3 text-center promoters">
			<h5>Promosso da</h5>
			<a href="https://www.ils.org/">
				<img class="img-responsive" src="<?php echo makeurl('/immagini/ils.png') ?>" alt="Italian Linux Society">
			</a>
		</div>

		<div class="col-md-9 main-contents">
<?php
}

function lugfooter () {
?>

		</div>
	</div>
</div>

<div style="clear: both; margin-bottom: 20px"></div>

<div id="ils_footer" class="mt-5">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<span style="text-align: center; display: block">
					<a href="http://www.gnu.org/licenses/agpl-3.0-standalone.html" rel="license">
						<img src="<?php echo makeurl('/immagini/agpl3.svg') ?>" style="border-width:0" alt="AGPLv3 License">
					</a>

					<a href="http://creativecommons.org/publicdomain/zero/1.0/deed.en_US" rel="license">
						<img src="<?php echo makeurl('/immagini/cczero.png') ?>" style="border-width:0" alt="Creative Commons License">
					</a>
				</span>
			</div>

			<div class="col-md-3">
				<h2>RESTA AGGIORNATO!</h2>
				<p>
					Iscriviti alla newsletter per aggiornamenti periodici sul software libero in Italia!
				</p>

				<p>
					Specificando la tua provincia di residenza riceverai anche gli annunci sulle <a href="https://www.linux.it/eventi">attività svolte dai LUG e dai gruppi amici</a> nella tua zona.
				</p>

				<form class="webform-client-form" action="https://www.linux.it/subscribe.php" method="get">
					<input type="email" class="form-control" name="name" placeholder="Indirizzo Mail" />
					<p style="display: none">
						<input type="text" name="mail" />
					</p>

					<?php prov_select ('form-control'); ?>

					<input type="submit" class="form-control" value="Iscriviti" />
				</form>
			</div>

			<div class="col-md-3">
				<h2>Amici</h2>
				<p style="text-align: center">
					<a href="http://www.ils.org/info#aderenti">
						<img src="https://www.ils.org/sites/ils.org/files/associazioni/getrand.php" border="0" /><br />
						Scopri tutte le associazioni che hanno aderito a ILS.
					</a>
				</p>
			</div>

			<div class="col-md-3">
				<h2>Network</h2>
				<script type="text/javaScript" src="https://www.linux.it/external/widgetils.php?referrer=linuxday"></script>
				<div id="widgetils"></div>
			</div>
		</div>
	</div>

	<div style="clear: both"></div>
</div>

<!-- Matomo -->
<script type="text/javascript">
	var _paq = window._paq || [];
	/* tracker methods like "setCustomDimension" should be called before "trackPageView" */
	_paq.push(["setCookieDomain", "*.www.linuxday.it"]);
	_paq.push(["setDomains", ["*.www.linuxday.it","*.www.linuxday.it"]]);
	_paq.push(['trackPageView']);
	_paq.push(['enableLinkTracking']);
	(function() {
		var u="//stats.madbob.org/";
		_paq.push(['setTrackerUrl', u+'matomo.php']);
		_paq.push(['setSiteId', '13']);
		var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
		g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
	})();
</script>
<!-- End Matomo Code -->

</body>
</html>

<?php
}

function prov_select($class, $selected = null) {
	?>

	<select class="<?php echo $class ?>" name="prov">
		<option value="-1" <?php echo ($selected == null ? 'selected' : '') ?>>Seleziona una Provincia</option>
		<option value="AG" <?php echo ($selected == 'AG' ? 'selected' : '') ?>>Agrigento</option>
		<option value="AL" <?php echo ($selected == 'AL' ? 'selected' : '') ?>>Alessandria</option>
		<option value="AN" <?php echo ($selected == 'AN' ? 'selected' : '') ?>>Ancona</option>
		<option value="AO" <?php echo ($selected == 'AO' ? 'selected' : '') ?>>Aosta</option>
		<option value="AR" <?php echo ($selected == 'AR' ? 'selected' : '') ?>>Arezzo</option>
		<option value="AP" <?php echo ($selected == 'AP' ? 'selected' : '') ?>>Ascoli Piceno</option>
		<option value="AT" <?php echo ($selected == 'AT' ? 'selected' : '') ?>>Asti</option>
		<option value="AV" <?php echo ($selected == 'AV' ? 'selected' : '') ?>>Avellino</option>
		<option value="BA" <?php echo ($selected == 'BA' ? 'selected' : '') ?>>Bari</option>
		<option value="BT" <?php echo ($selected == 'BT' ? 'selected' : '') ?>>Barletta-Andria-Trani</option>
		<option value="BL" <?php echo ($selected == 'BL' ? 'selected' : '') ?>>Belluno</option>
		<option value="BN" <?php echo ($selected == 'BN' ? 'selected' : '') ?>>Benevento</option>
		<option value="BG" <?php echo ($selected == 'BG' ? 'selected' : '') ?>>Bergamo</option>
		<option value="BI" <?php echo ($selected == 'BI' ? 'selected' : '') ?>>Biella</option>
		<option value="BO" <?php echo ($selected == 'BO' ? 'selected' : '') ?>>Bologna</option>
		<option value="BZ" <?php echo ($selected == 'BZ' ? 'selected' : '') ?>>Bolzano</option>
		<option value="BS" <?php echo ($selected == 'BS' ? 'selected' : '') ?>>Brescia</option>
		<option value="BR" <?php echo ($selected == 'BR' ? 'selected' : '') ?>>Brindisi</option>
		<option value="CA" <?php echo ($selected == 'CA' ? 'selected' : '') ?>>Cagliari</option>
		<option value="CL" <?php echo ($selected == 'CL' ? 'selected' : '') ?>>Caltanissetta</option>
		<option value="CB" <?php echo ($selected == 'CB' ? 'selected' : '') ?>>Campobasso</option>
		<option value="CI" <?php echo ($selected == 'CI' ? 'selected' : '') ?>>Carbonia-Iglesias</option>
		<option value="CE" <?php echo ($selected == 'CE' ? 'selected' : '') ?>>Caserta</option>
		<option value="CT" <?php echo ($selected == 'CT' ? 'selected' : '') ?>>Catania</option>
		<option value="CZ" <?php echo ($selected == 'CZ' ? 'selected' : '') ?>>Catanzaro</option>
		<option value="CH" <?php echo ($selected == 'CH' ? 'selected' : '') ?>>Chieti</option>
		<option value="CO" <?php echo ($selected == 'CO' ? 'selected' : '') ?>>Como</option>
		<option value="CS" <?php echo ($selected == 'CS' ? 'selected' : '') ?>>Cosenza</option>
		<option value="CR" <?php echo ($selected == 'CR' ? 'selected' : '') ?>>Cremona</option>
		<option value="KR" <?php echo ($selected == 'KR' ? 'selected' : '') ?>>Crotone</option>
		<option value="CN" <?php echo ($selected == 'CN' ? 'selected' : '') ?>>Cuneo</option>
		<option value="EN" <?php echo ($selected == 'EN' ? 'selected' : '') ?>>Enna</option>
		<option value="FM" <?php echo ($selected == 'FM' ? 'selected' : '') ?>>Fermo</option>
		<option value="FE" <?php echo ($selected == 'FE' ? 'selected' : '') ?>>Ferrara</option>
		<option value="FI" <?php echo ($selected == 'FI' ? 'selected' : '') ?>>Firenze</option>
		<option value="FG" <?php echo ($selected == 'FG' ? 'selected' : '') ?>>Foggia</option>
		<option value="FC" <?php echo ($selected == 'FC' ? 'selected' : '') ?>>Forl&igrave;-Cesena</option>
		<option value="FR" <?php echo ($selected == 'FR' ? 'selected' : '') ?>>Frosinone</option>
		<option value="GE" <?php echo ($selected == 'GE' ? 'selected' : '') ?>>Genova</option>
		<option value="GO" <?php echo ($selected == 'GO' ? 'selected' : '') ?>>Gorizia</option>
		<option value="GR" <?php echo ($selected == 'GR' ? 'selected' : '') ?>>Grosseto</option>
		<option value="IM" <?php echo ($selected == 'IM' ? 'selected' : '') ?>>Imperia</option>
		<option value="IS" <?php echo ($selected == 'IS' ? 'selected' : '') ?>>Isernia</option>
		<option value="SP" <?php echo ($selected == 'SP' ? 'selected' : '') ?>>La Spezia</option>
		<option value="AQ" <?php echo ($selected == 'AQ' ? 'selected' : '') ?>>L'Aquila</option>
		<option value="LT" <?php echo ($selected == 'LT' ? 'selected' : '') ?>>Latina</option>
		<option value="LE" <?php echo ($selected == 'LE' ? 'selected' : '') ?>>Lecce</option>
		<option value="LC" <?php echo ($selected == 'LC' ? 'selected' : '') ?>>Lecco</option>
		<option value="LI" <?php echo ($selected == 'LI' ? 'selected' : '') ?>>Livorno</option>
		<option value="LO" <?php echo ($selected == 'LO' ? 'selected' : '') ?>>Lodi</option>
		<option value="LU" <?php echo ($selected == 'LU' ? 'selected' : '') ?>>Lucca</option>
		<option value="MC" <?php echo ($selected == 'MC' ? 'selected' : '') ?>>Macerata</option>
		<option value="MN" <?php echo ($selected == 'MN' ? 'selected' : '') ?>>Mantova</option>
		<option value="MS" <?php echo ($selected == 'MS' ? 'selected' : '') ?>>Massa-Carrara</option>
		<option value="MT" <?php echo ($selected == 'MT' ? 'selected' : '') ?>>Matera</option>
		<option value="ME" <?php echo ($selected == 'ME' ? 'selected' : '') ?>>Messina</option>
		<option value="MI" <?php echo ($selected == 'MI' ? 'selected' : '') ?>>Milano</option>
		<option value="MO" <?php echo ($selected == 'MO' ? 'selected' : '') ?>>Modena</option>
		<option value="MB" <?php echo ($selected == 'MB' ? 'selected' : '') ?>>Monza e Brianza</option>
		<option value="NA" <?php echo ($selected == 'NA' ? 'selected' : '') ?>>Napoli</option>
		<option value="NO" <?php echo ($selected == 'NO' ? 'selected' : '') ?>>Novara</option>
		<option value="NU" <?php echo ($selected == 'NU' ? 'selected' : '') ?>>Nuoro</option>
		<option value="OT" <?php echo ($selected == 'OT' ? 'selected' : '') ?>>Olbia-Tempio</option>
		<option value="OR" <?php echo ($selected == 'OR' ? 'selected' : '') ?>>Oristano</option>
		<option value="PD" <?php echo ($selected == 'PD' ? 'selected' : '') ?>>Padova</option>
		<option value="PA" <?php echo ($selected == 'PA' ? 'selected' : '') ?>>Palermo</option>
		<option value="PR" <?php echo ($selected == 'PR' ? 'selected' : '') ?>>Parma</option>
		<option value="PV" <?php echo ($selected == 'PV' ? 'selected' : '') ?>>Pavia</option>
		<option value="PG" <?php echo ($selected == 'PG' ? 'selected' : '') ?>>Perugia</option>
		<option value="PU" <?php echo ($selected == 'PU' ? 'selected' : '') ?>>Pesaro e Urbino</option>
		<option value="PE" <?php echo ($selected == 'PE' ? 'selected' : '') ?>>Pescara</option>
		<option value="PC" <?php echo ($selected == 'PC' ? 'selected' : '') ?>>Piacenza</option>
		<option value="PI" <?php echo ($selected == 'PI' ? 'selected' : '') ?>>Pisa</option>
		<option value="PT" <?php echo ($selected == 'PT' ? 'selected' : '') ?>>Pistoia</option>
		<option value="PN" <?php echo ($selected == 'PN' ? 'selected' : '') ?>>Pordenone</option>
		<option value="PZ" <?php echo ($selected == 'PZ' ? 'selected' : '') ?>>Potenza</option>
		<option value="PO" <?php echo ($selected == 'PO' ? 'selected' : '') ?>>Prato</option>
		<option value="RG" <?php echo ($selected == 'RG' ? 'selected' : '') ?>>Ragusa</option>
		<option value="RA" <?php echo ($selected == 'RA' ? 'selected' : '') ?>>Ravenna</option>
		<option value="RC" <?php echo ($selected == 'RC' ? 'selected' : '') ?>>Reggio Calabria</option>
		<option value="RE" <?php echo ($selected == 'RE' ? 'selected' : '') ?>>Reggio Emilia</option>
		<option value="RI" <?php echo ($selected == 'RI' ? 'selected' : '') ?>>Rieti</option>
		<option value="RN" <?php echo ($selected == 'RN' ? 'selected' : '') ?>>Rimini</option>
		<option value="RM" <?php echo ($selected == 'RM' ? 'selected' : '') ?>>Roma</option>
		<option value="RO" <?php echo ($selected == 'RO' ? 'selected' : '') ?>>Rovigo</option>
		<option value="SA" <?php echo ($selected == 'SA' ? 'selected' : '') ?>>Salerno</option>
		<option value="VS" <?php echo ($selected == 'VS' ? 'selected' : '') ?>>Medio Campidano</option>
		<option value="SS" <?php echo ($selected == 'SS' ? 'selected' : '') ?>>Sassari</option>
		<option value="SV" <?php echo ($selected == 'SV' ? 'selected' : '') ?>>Savona</option>
		<option value="SI" <?php echo ($selected == 'SI' ? 'selected' : '') ?>>Siena</option>
		<option value="SR" <?php echo ($selected == 'SR' ? 'selected' : '') ?>>Siracusa</option>
		<option value="SO" <?php echo ($selected == 'SO' ? 'selected' : '') ?>>Sondrio</option>
		<option value="TA" <?php echo ($selected == 'TA' ? 'selected' : '') ?>>Taranto</option>
		<option value="TE" <?php echo ($selected == 'TE' ? 'selected' : '') ?>>Teramo</option>
		<option value="TR" <?php echo ($selected == 'TR' ? 'selected' : '') ?>>Terni</option>
		<option value="TO" <?php echo ($selected == 'TO' ? 'selected' : '') ?>>Torino</option>
		<option value="OG" <?php echo ($selected == 'OG' ? 'selected' : '') ?>>Ogliastra</option>
		<option value="TP" <?php echo ($selected == 'TP' ? 'selected' : '') ?>>Trapani</option>
		<option value="TN" <?php echo ($selected == 'TN' ? 'selected' : '') ?>>Trento</option>
		<option value="TV" <?php echo ($selected == 'TV' ? 'selected' : '') ?>>Treviso</option>
		<option value="TS" <?php echo ($selected == 'TS' ? 'selected' : '') ?>>Trieste</option>
		<option value="UD" <?php echo ($selected == 'UD' ? 'selected' : '') ?>>Udine</option>
		<option value="VA" <?php echo ($selected == 'VA' ? 'selected' : '') ?>>Varese</option>
		<option value="VE" <?php echo ($selected == 'VE' ? 'selected' : '') ?>>Venezia</option>
		<option value="VB" <?php echo ($selected == 'VB' ? 'selected' : '') ?>>Verbano-Cusio-Ossola</option>
		<option value="VC" <?php echo ($selected == 'VC' ? 'selected' : '') ?>>Vercelli</option>
		<option value="VR" <?php echo ($selected == 'VR' ? 'selected' : '') ?>>Verona</option>
		<option value="VV" <?php echo ($selected == 'VV' ? 'selected' : '') ?>>Vibo Valentia</option>
		<option value="VI" <?php echo ($selected == 'VI' ? 'selected' : '') ?>>Vicenza</option>
		<option value="VT" <?php echo ($selected == 'VT' ? 'selected' : '') ?>>Viterbo</option>
	</select>

	<?php
}
