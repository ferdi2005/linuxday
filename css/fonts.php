<?php

$file = basename($_GET['font']);
if (!file_exists($file))
	die();

header("Access-Control-Allow-Origin: *");
header("Content-Type: " . mime_content_type($file));
echo @file_get_contents($file);
