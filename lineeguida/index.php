<?php
/*
Copyright (C) 2019  Italian Linux Society - http://www.linux.it

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

require_once ('../funzioni.php');
lugheader ('Linux Day ' . conf('current_year') . ': Linee Guida');

?>

<h2>Linee Guida</h2>

<p>
	Versione sintetica, per chi ha fretta.
</p>

<ul>
	<li>Il Linux Day <?php echo conf('current_year') ?> si terrà <?php echo strtolower(conf('human_date')) ?></li>
	<li>Il Linux Day è una manifestazione nazionale unitaria articolata in varie manifestazioni locali</li>
	<li>Essendo volto a favorire un'ampia diffusione e conoscenza di GNU/Linux e del software libero, il Linux Day si rivolge principalmente al grande pubblico</li>
	<li>Il Linux Day ha lo scopo di promuovere l'uso e la conoscenza del sistema operativo Linux e del software libero</li>
	<li>L'accesso alla manifestazione deve sempre essere libero e gratuito</li>
	<li>Agli organizzatori locali è lasciata la possibilità di eventuali raccolte fondi</li>
</ul>

<p>
	Versione estesa, per chi vuole saperne di più.
</p>

<ol>
	<li>
		il Linux Day ha lo scopo di promuovere l'uso e la conoscenza del sistema operativo GNU/Linux e del software libero. è eventualmente possibile prendere in considerazione anche altri sistemi operativi liberi, purché la manifestazione sia comunque incentrata su GNU/Linux. Il fine è la promozione di GNU/Linux in quanto software libero, e non la promozione di qualunque programma che giri su GNU/Linux; è comunque possibile accennare marginalmente a software proprietario relativo a GNU/Linux per completezza di informazione, ad esempio per questioni di interoperabilità, o in mancanza di un equivalente libero eccetera, purché tale accenno sia motivato rispetto allo scopo della manifestazione e non vi sia assolutamente l'intento di promuovere tale software. è inoltre possibile accennare alla compatibilità con sistemi proprietari relativamente a software libero disponibile, oltre che per GNU/Linux, anche per tali sistemi. In ogni caso la responsabilità di ciò che viene mostrato, se non fa parte dei materiali comuni, ricade esclusivamente sul singolo gruppo locale (ad esempio per questioni di licenze) e non su ILS o sui restanti organizzatori
	</li>
	<li>
		Essendo volto a favorire un'ampia diffusione e conoscenza di GNU/Linux e del software libero, il Linux Day si rivolge principalmente al grande pubblico. Questo non esclude la possibilità di trattare argomenti avanzati e specialistici, tuttavia è necessario non trascurare attività e interventi destinati agli utenti meno esperti, sia per quanto riguarda gli aspetti tecnici che il concetto stesso di software libero.
	</li>
	<li>
		L'accesso alla manifestazione deve sempre essere libero e gratuito , in ogni sua parte e momento. Non sono ammesse forme di ingresso a pagamento o dietro iscrizioni obbligatorie, qualunque sia il gruppo o associazione interessato.
	</li>
	<li>
		L'organizzazione della manifestazione non deve essere in alcun modo legata ad attività di gruppi politici, religiosi o di qualunque altro tipo che non perseguano le finalità della manifestazione stessa. Non è quindi accettabile che tali soggetti facciano parte dell'organizzazione, che ospitino la manifestazione (o parte di essa) all'interno di proprie iniziative o collegandola con proprie iniziative e attività, o che intervengano durante la stessa se non per perseguire gli obiettivi della manifestazione come esposti al punto 1. Questo non esclude eventuali rapporti con figure istituzionali.
	</li>
	<li>
		Agli organizzatori locali è lasciata la possibilità di eventuali raccolte fondi, ad esempio con offerte volontarie, vendita di gadget o raccolta di nuove iscrizioni, da gestire in proprio. Qualunque responsabilità in tal senso, ad esempio per questioni fiscali, ricade sul relativo gruppo locale e non sui restanti organizzatori. Eventuali gadget o materiali forniti gratuitamente a livello nazionale tramite ILS per la distribuzione durante la manifestazione devono comunque essere distribuiti gratuitamente: non possono quindi essere venduti, nè è possibile distribuirli in cambio di un'offerta o dell'iscrizione a un'associazione.
	</li>
	<li>
		È ammessa la possibilità di sponsorizzazioni della manifestazione, sia a livello nazionale che per i singoli eventi locali, con le medesime regole. L'eventuale presenza degli sponsor, sia nazionali che locali, deve comunque essere in linea con gli obiettivi della manifestazione come esposti al punto 1 e deve essere salvaguardato il carattere non commerciale, plurale e indipendente della manifestazione stessa. Al fine di evitare che un evento locale sia focalizzato sulla presentazione di una particolare azienda e/o dei suoi prodotti e servizi, il che non è ammissibile, dovranno essere rispettati i seguenti punti
		<ul>
			<li>sono ammesse donazioni in denaro, da qualsiasi ente o azienda, all'organizzatore;</li>
			<li>sono ammesse donazioni in materiale attinente al software libero (cdrom, libri, documentazione, gadget, magliette, penne, cartelline...);</li>
			<li>non è ammessa la distribuzione di materiale che non riguardi il software libero;</li>
			<li>uno sponsor non può cedere ad altri enti, aziende o marchi i diritti derivanti dalla sponsorizzazione dell'evento senza prima aver interpellato gli organizzatori (locali o nazionali) di riferimento.</li>
		</ul>
	</li>
	<li>
		Il Linux Day è una manifestazione nazionale unitaria articolata in varie manifestazioni locali. è necessario che l'aspetto unitario sia evidente e venga sottolineato, utilizzando sempre e solo la denominazione ufficiale "Linux Day <?php echo conf('current_year') ?> - diciottesima giornata nazionale di Linux e del software libero" (brevemente "Linux Day <?php echo conf('current_year') ?>", "Linux Day", o in sigla "LD18"), inserendo nel sito web e sul materiale dedicato alla manifestazione locale il riferimento al sito web nazionale, e utilizzando logo, manifesto, volantini e altro materiale comune che potrà essere messo a disposizione.
	</li>
	<li>
		Il Linux Day <?php echo conf('current_year') ?> si terrà <?php echo strtolower(conf('human_date')) ?>. Ciascun gruppo organizzatore ha facoltà di organizzare anche eventi collaterali legati al Linux Day nei giorni dal giovedì precedente alla domenica successiva, purché l'evento principale si tenga <?php echo strtolower(conf('human_date')) ?>. L'evento principale e quelli collaterali ad esso legati possono anche svolgersi in località vicine; è comunque necessario che il gruppo organizzatore sia unificato e che gli eventi vengano gestiti e pubblicizzati in maniera unitaria e secondo un programma unico. Anche gli eventi collaterali devono essere conformi alle linee guida qui specificate.
	</li>
	<li>
		Affinché il proprio evento locale sia incluso nella manifestazione è necessario darne comunicazione sufficientemente dettagliata, includendo il proprio programma di massima, a ILS entro e non oltre il 10 ottobre <?php echo conf('current_year') ?>. ILS si riserva la facoltà di non includere eventi che, a proprio giudizio, appaiano non conformi con lo spirito della manifestazione e con le presenti linee guida, nonché eventi organizzati da gruppi che, in precedenti edizioni, abbiano organizzato eventi non conformi con lo spirito della manifestazione.
	</li>
</ol>

<?php
lugfooter ();

